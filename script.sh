#!/usr/bin/env bash

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install dialog apt-utils
sudo apt-get install -y unzip
sudo apt-get install python3-virtualenv 
sudo apt-get install python3-pip  -y 
pip3 install flask
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update -y
sudo apt-get install postgresql -y 


sudo -i -u postgres psql -c "CREATE USER user WITH PASSWORD 'password';"
mkdir credentials
cd credentials
echo "db_user = user , db_password = 'password'" > .env
cat .env